# Build Gaussino with LCG 103

Checkout the repo:
```bash
$ git clone ssh://git@gitlab.cern.ch:7999/talin/build-cepc-on-gaussino.git
$ cd build-cepc-on-gaussino
```

Build Gaussino:
```bash
$ bash build.sh build-all
```

Setup the runtime environment:
```bash
$ source setup.sh
```

Run Gaussino:
```bash
$ gaudirun.py my_gaussino.py
```

