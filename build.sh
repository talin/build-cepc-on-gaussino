#!/bin/bash
# Description: build cepc-on-gaussino
# Author: Tao Lin

function env-setup() {
    # == LCG  ==
    lcg_version_lcg=LCG_103 # The version used by LCG self.
    lcg_platform=x86_64-centos7-gcc11-opt

    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/git/bin:$PATH
    export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/contrib/git/lib64:$LD_LIBRARY_PATH
    
    source /cvmfs/sft.cern.ch/lcg/views/${lcg_version_lcg}/${lcg_platform}/setup.sh

    # == Gaussino and dependencies  ==

    export WORKTOP=$(pwd)

    for project in LHCb gaussinoextlibs Gaussino CEPCSW; do
        
        project_path=$WORKTOP/$project/InstallArea
        export CMAKE_PREFIX_PATH=$project_path:$CMAKE_PREFIX_PATH
        export PATH=$project_path/bin:$PATH
        export LD_LIBRARY_PATH=$project_path/lib:$LD_LIBRARY_PATH
        export PYTHONPATH=$project_path/lib:$PYTHONPATH
        export PYTHONPATH=$project_path/python:$PYTHONPATH
    
    done

}

env-setup

function build-pkg() {
    local project=$1
    cd $WORKTOP
    git clone ssh://git@gitlab.cern.ch:7999/talin/${project}.git
    cd $project
    git checkout cepc-on-gaussino

    cmake -S . -B build -DUSE_DD4HEP=ON -DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX=$(pwd)/InstallArea
    cmake --build build -j16
    cmake --install build
}

function build-lhcb() {
    build-pkg LHCb
}

function build-gaussinoextlibs() {
    build-pkg gaussinoextlibs
}

function build-gaussino() {
    build-pkg Gaussino
}

function build-cepcsw() {
    build-pkg CEPCSW
}

function build-all() {
    (build-lhcb)
    (build-gaussinoextlibs)
    (build-gaussino)
    (build-cepcsw)
}

$*
