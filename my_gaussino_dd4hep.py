from Gaudi.Configuration import DEBUG
from Configurables import Gaussino
from Configurables import GiGaMT

giga = GiGaMT()

Gaussino().EvtMax = 1000

Gaussino().Phases = ["Generator", "Simulation"]

from GaudiKernel import SystemOfUnits as units
from Configurables import (
    GaussinoGeneration,
    GaussinoSimulation,
    ParticleGun,
    FixedMomentum,
    MomentumRange,
    FlatPtRapidity,
    FlatNParticles,
)

GaussinoGeneration().ParticleGun = True
pgun = ParticleGun("ParticleGun")
# pgun.addTool(FixedMomentum, name="FixedMomentum")
# pgun.FixedMomentum.px = 1.0 * units.GeV
# pgun.FixedMomentum.py = 0.0 * units.GeV
# pgun.FixedMomentum.pz = 0.0 * units.GeV
# pgun.FixedMomentum.PdgCodes = [11]
# pgun.ParticleGunTool = "FixedMomentum"

pgun.addTool(MomentumRange, name="MomentumRange")
pgun.ParticleGunTool = "MomentumRange"
pgun.MomentumRange.MomentumMin = 100*units.GeV
pgun.MomentumRange.MomentumMax = 100*units.GeV
pgun.MomentumRange.ThetaMin = 90*units.deg
pgun.MomentumRange.ThetaMax = 90*units.deg
pgun.MomentumRange.PdgCodes = [11]


# pgun.addTool(FlatPtRapidity, name="FlatPtRapidity")
# pgun.ParticleGunTool = "FlatPtRapidity"
# pgun.FlatPtRapidity.PtMin = 100*units.GeV
# pgun.FlatPtRapidity.PtMax = 100*units.GeV
# pgun.FlatPtRapidity.PdgCodes = [11]

pgun.addTool(FlatNParticles, name="FlatNParticles")
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.FlatNParticles.MinNParticles = 1
pgun.FlatNParticles.MaxNParticles = 1

GaussinoSimulation().PhysicsConstructors.append("GiGaMT_G4EmStandardPhysics")
GaussinoSimulation().PhysicsConstructors.append("GiGaMT_G4HadronPhysicsFTFP_BERT")
GaussinoSimulation().PhysicsConstructors.append("GiGaMT_G4DecayPhysics")

GaussinoSimulation().TrackTruth = False

from Configurables import (
    GaussinoGeometry,
    DD4hepCnvSvc,
    GenericTrackerMonTool,
)

montool = giga.addTool(GenericTrackerMonTool, name="GenericTrackerMonTool")
montool.OutputLevel = DEBUG
montool.CollectionName = "VXDCollection"

giga.MonitorTools = ["GenericTrackerMonTool"]

GaussinoGeometry().GeometryService = "DD4hepCnvSvc"
# DD4hepCnvSvc().DescriptionLocation = "compact/SiD.xml"
DD4hepCnvSvc().DescriptionLocation = "CEPCSW/Detector/DetCRD/compact/CRD_o1_v03/CRD_o1_v03-onlyVXD.xml"
DD4hepCnvSvc().SensDetMappings = {"VXD": "VXD"}
# DD4hepCnvSvc().OutputLevel = DEBUG
