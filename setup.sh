#!/bin/bash

function env-setup() {
    # == LCG  ==
    lcg_version_lcg=LCG_103 # The version used by LCG self.
    lcg_platform=x86_64-centos7-gcc11-opt

    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/git/bin:$PATH
    export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/contrib/git/lib64:$LD_LIBRARY_PATH
    
    source /cvmfs/sft.cern.ch/lcg/views/${lcg_version_lcg}/${lcg_platform}/setup.sh

    # == Gaussino and dependencies  ==

    export WORKTOP=$(pwd)

    for project in LHCb gaussinoextlibs Gaussino CEPCSW; do
        
        project_path=$WORKTOP/$project/InstallArea
        export CMAKE_PREFIX_PATH=$project_path:$CMAKE_PREFIX_PATH
        export PATH=$project_path/bin:$PATH
        export LD_LIBRARY_PATH=$project_path/lib:$LD_LIBRARY_PATH
        export PYTHONPATH=$project_path/lib:$PYTHONPATH
        export PYTHONPATH=$project_path/python:$PYTHONPATH
    
    done

    export GAUSSINOROOT=$WORKTOP/Gaussino/Sim/Gaussino
}

env-setup
